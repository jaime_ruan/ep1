#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>
#include "pessoa.hpp"

using namespace std;

class Cliente : public Pessoa{
private:
    string data_de_cadastro;
public:
    Cliente();
    Cliente(long int cpf, string nome);
    Cliente(long int cpf, string nome, string telefone, string email, string data_de_cadastro);
    ~Cliente();
    void set_data_de_cadastro(string data_de_cadastro);
    string get_data_de_cadastro();
    void imprime_dados();
};

#endif