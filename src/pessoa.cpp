#include "pessoa.hpp"
#include <iostream>

using namespace std;

Pessoa::Pessoa(){
    cout << "Construtor da classe Pessoa" << endl;
    set_cpf(0);
    set_nome("");
    set_telefone("");
    set_email(""); 
    cout << "";   
}
Pessoa::~Pessoa(){
    cout << "Destrutor da classe Pessoa" << endl;
}

long int Pessoa::get_cpf(){
    return cpf;
}
void Pessoa::set_cpf(long int cpf){
    this->cpf = cpf;
}string Pessoa::get_nome(){
    return nome;
}
void Pessoa::set_nome(string nome){
    this->nome = nome;
}
string Pessoa::get_telefone(){
    return telefone;
}
void Pessoa::set_telefone(string telefone){
    this->telefone = telefone;
}
string Pessoa::get_email(){
    return email;
}   
void Pessoa::set_email(string email){
    this->email = email;
}
void Pessoa::imprime_dados(){
    cout << "CPF: " << get_cpf() << endl;
    cout << "Nome: " << get_nome() << endl;
    cout << "Telefone: " << get_telefone() << endl;    
    cout << "Email: " << get_email() << endl;
} 