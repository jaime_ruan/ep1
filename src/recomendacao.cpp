// *** Modo Recomendação
// Para listar os itens recomendados para cada cliente, Victoria
// pensou em uma solução bem simples. A cada compra, é possível
// identificar a categoria de cada produto. Com este dado, é possível
// saber qual categoria que mais interessa o cliente.
// Neste modo, também há alguns pontos a serem considerados:
// - Ao entrar no modo recomendação, deve ser possível inserir
// os dados do cliente para buscar os produtos recomendados
// exclusivamente;
// - Caso o cliente não possua cadastro, uma mensagem de erro
// deve sermostrada;
// - A lista de recomendações deve ter as seguintescaracterísticas:
// - Até 10produtos;
// - Ordenados de acordo com o grau de recomendação
// (mais recomendados primeiro, menos recomendados porúltimo);
// - Caso o grau de recomendação seja o mesmo, o critério
// de ordenação deve obedecer à ordemlexicográfica;

// #include "recomendacao.hpp"
// #include <iostream>