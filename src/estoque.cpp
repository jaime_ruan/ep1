// ** Modo Estoque
// Para manter o estoque do estabelecimento, deve ser possível
// cadastrar novos produtos (não haverá a necessidade de se remover
// produtos, apenas de se atualizar sua quantidade). Além disso, para
// evitar o recadastro de produtos e categorias, os dados devem ser
// armazenados em arquivos.
// Aqui estão alguns aspectos importantes deste modo:
// - Há várias categorias de produtos existentes no
// estabelecimento e, sempre que possível, Victoria tenta trazer
// coisas novas (O número de categorias não é fixo mas, assim como
// no caso dos produtos, não será necessário remover nenhuma categoria);
// - Um produto pode pertencer a mais de uma categoria;

// #include "estoque.hpp"
// #include <iostream>