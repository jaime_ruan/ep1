#include "cliente.hpp"
#include <iostream>

Cliente::Cliente(){
    set_cpf(0);
    std::cout << "";
    set_nome("");
    set_telefone("");
    set_email("email.com");
    set_data_de_cadastro("");
    std::cout << "Construtor da classe Cliente" << std::endl; 
}

Cliente::Cliente(long int cpf, string nome){
    set_cpf(cpf);
    set_nome(nome);
    set_telefone("");
    set_email("email.com");
    set_data_de_cadastro("");
    std::cout << "Construtor da classe Cliente - Sobrecarga 1" << std::endl; 
}

Cliente::Cliente(long int cpf, string nome, string telefone, string email, string data_de_cadastro){
    set_cpf(cpf);
    set_nome(nome);
    set_telefone(telefone);
    set_email(email);
    set_data_de_cadastro(data_de_cadastro);
    std::cout << "Construtor da classe Cliente - Sobrecarga 2" << std::endl;     
}
    

Cliente::~Cliente(){
    std::cout << "Destrutor da classe Cliente"<< std::endl;
}
void Cliente::set_data_de_cadastro(string data_de_cadastro){
    this->data_de_cadastro = data_de_cadastro;
}
string Cliente::get_data_de_cadastro(){
    return data_de_cadastro;
}

void Cliente::imprime_dados(){
    cout << "CPF: " << get_cpf() << endl;
    cout << "Nome: " << get_nome() << endl;
    cout << "Telefone: " << get_telefone() << endl;    
    cout << "Email: " << get_email() << endl;
    cout << "Data de Cadastro: " << get_data_de_cadastro() << endl;
}