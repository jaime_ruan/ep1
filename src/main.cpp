#include <iostream>
#include <cstdlib>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include "estoque.hpp"
#include "recomendacao.hpp"
#include "venda.hpp"
#include "pessoa.hpp"
#include "cliente.hpp"
#include "socio.hpp"

using namespace std;

string getString(){
    string valor;
    getline(cin, valor);
    return valor;
}

template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida! Insira novamente: " << endl;
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}

int main(){
    
    int modo1 = -1, modo2 = -1;
    char ans1, ans2, ans3;

    vector<Cliente * > clientes;

//    vector<Socio * > socios;

    vector<Pessoa *> pessoas;

//    vector<Venda * > vendas;

//    vector<Recomendacao * >recomendacoes;

    cout << "" << endl;
    cout << "€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€" << endl;
    cout << "»»»»»»»»»»»»» BEM VINDO AO SOFTWARE DE MERCADO VICTORIA «««««««««««««" << endl;
    cout << "€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€€" << endl;
    cout << "" << endl;

    while (modo1 != 0){
        cout << "Digite o modo que desejas ingressar:" << endl;
        cout << "(1) Modo Venda | (2) Modo Recomendação | (3) Modo Estoque | (0) Sair" << endl;
        modo1 = getInput<int>();

        string nome, telefone, email, data_de_cadastro;
        long int cpf;

        switch (modo1){
            case 0:
                break;
            case 1:
                system("clear");

                cout << "Já tem cadastro no sistema? [y/n]" << endl;
                cin >> ans1;
                
                again:
                
                if(ans1 == 'y' || ans1 =='Y'){
                    cout << "É sócio? [y/n]" << endl;
                    cin >> ans2;
                    again2:
                    if (ans2 == 'y' || ans2 =='Y'){
                        goto venda_socio;
                    } else if (ans2 == 'n' || ans2 =='N'){
                        goto venda_cliente;
                    }else{
                        cout << "Entrada inválida! Insira novamente: " << endl;   
                        goto again2;
                    }
                }else if (ans1 == 'n' || ans1 =='N'){
                        goto cadastro;
                }else{
                        cout << "Entrada inválida! Insira novamente: " << endl;   
                        goto again;
                }

                cadastro:
                
                system("clear");

                cout << "Cadastre o cliente: " << endl;

                cout << "CPF: ";
                cpf = getInput<long int>();

                cout << "Nome: ";
                nome = getString();
                        
                cout << "Telefone: ";
                telefone = getString();
                        
                cout << "Email: ";
                email = getString();

                cout << "Data de cadastro: ";
                data_de_cadastro = getString();

                clientes.push_back(new Cliente(cpf, nome , telefone, email, data_de_cadastro));
                pessoas.push_back(new Cliente(cpf, nome, telefone, email, data_de_cadastro));

                cout << "Deseja ser sócio? [y/n]" << endl;
                cin >> ans3;
                    again3:
                    if (ans3 == 'y' || ans3 =='Y'){
                        goto venda_socio;
                    } else if (ans3 == 'n' || ans3 =='N'){
                        goto venda_cliente;
                    }else{
                        cout << "Entrada inválida! Insira novamente: " << endl;   
                        goto again3;
                    }
                    
                
                venda_cliente:
                system("clear");
                    cout << "Venda Cliente" << endl; 
                    //implementar;
                venda_socio:
                system("clear");
                    cout << "Venda Socio" << endl;
                    //implementar;

                remove("doc/carrinho.txt");
                
                break;
            case 2:
                system("clear");

                cout << "(1) Conferir Estoque | (2) Cadastrar Produto | (0) Sair" << endl;
                modo2 = getInput<int>();

                while(modo2 != 0){
                    cout << "Digite o modo que desejas ingressar:" << endl;
                    cout << "(1) Conferir Estoque | (2) Cadastrar Produto | (0) Sair" << endl;
                    modo2 = getInput<int>();

                    switch (modo2){
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    default:
                        break;
                    }
                }
                break;
            case 3:
                system("clear");

                cout << "Digite o cpf do cliente: " << endl;
                //implementar
                break;
            default:
                cout << "Entrada inválida! Insira novamente: " << endl;
        }
    }
    return 0;
}